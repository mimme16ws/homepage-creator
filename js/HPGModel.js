var HPGModel = HPGModel || {};
HPGModel = (function () {
    "use strict";
    /* eslint-env browser  */
    /* globals HPGModel, EventPublisher */

    var that = new EventPublisher(),
        view,
        headerButton,
        textButton,
        btrapCSS,
        btrapMAP,
        fAwe,
        exporting,
        exportButton,
        emailButton,
        pageButton,
        colorButton,
        bgColor,
        done1 = false,
        done2 = false,
        done3 = false;

    //exportiert die Homepage
    function exportToZip(doc) {
        var site = new JSZip(),
            content;
        site.file("test.html", doc);
        site.file("css/bootstrap.min.css", btrapCSS);
        site.file("css/bootstrap.min.css.map", btrapMAP);
        site.file("css/fontAwesome/css/font-awesome.css", fAwe);
        content = site.generate({
            type: "blob"
        });
        saveAs(content, "test.zip");
    }

    //Erschafft die CSS dateien für den export
    function fillCSS() {
        var client1 = new XMLHttpRequest(),
            client2 = new XMLHttpRequest(),
            client3 = new XMLHttpRequest();

        client1.open("GET", "./css/bootstrap.min.css");
        client2.open("GET", "./css/bootstrap.min.css.map");
        client3.open("GET", "./css/fontAwesome/css/font-awesome.css");

        client1.onreadystatechange = function () {

            if (client1.readyState == 4 && client1.status == 200) {

                btrapCSS = client1.responseText;
                done1 = true;
                onAjaxCallFinished();
            }
        }


        client1.send();

        client2.onreadystatechange = function () {

            if (client2.readyState == 4 && client2.status == 200) {
                btrapMAP = client2.responseText;
                done2 = true;
                onAjaxCallFinished();
            }
        }

        client2.send();

        client3.onreadystatechange = function () {

            if (client3.readyState == 4 && client3.status == 200) {
                fAwe = client3.responseText;
                done3 = true;
                onAjaxCallFinished();
            }
        }

        client3.send();


    }

    function onAjaxCallFinished() {

        if (done1 == true && done2 == true && done3 == true) {

            exportToZip(exporting);
            done1 = false;
            done2 = false;
            done3 = false;
            exporting = "";
        }
    }

    /*function onPageClicked(event) {
        view.createLayout(event.data);
    }*/

    function onMailClicked(event) {
        view.createNewMail(event.data);
    }

    // Holt sich die Divs und erstellt einen String mit den Inhalten
    function onExportClicked() {
        var mainDiv = document.getElementById("containerDiv"),
            color = document.getElementById("colorPick"),
            childCount = mainDiv.childElementCount,
            actualDiv,
            i;
        bgColor = color.value;
        console.log(bgColor);

        exporting = "<head><meta charset=\"utf-8\"> <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"><link href=\"css/bootstrap.min.css\" rel=\"stylesheet\" /> <link href=\"css/bootstrap.min.css.map\" rel=\"stylesheet\" /><link href=\"css/fontAwesome/css/font-awesome.css\" rel=\"stylesheet\" /></head><body bgcolor=" + bgColor + ">";

        //backgroundcolor

        for (i = 0; i < childCount; i++) {
            actualDiv = mainDiv.children[i].children[0];

            if (actualDiv != null) {
                if (actualDiv.getAttribute("id").includes("Textfeld")) {
                    exporting += "<p>" + actualDiv.children[1].innerHTML + "</p>";
                } else if (actualDiv.getAttribute("id").includes("header")) {
                    exporting += "<h1 class = \"center\">" + actualDiv.children[0].value + "</h1>";
                } else if (actualDiv.getAttribute("id").includes("container")) {
                    exporting += $("div.containerDiv").html();


                }
            }
        }


        exporting += "</body>";



        fillCSS();
        //;
    }

    function onColorButtonClicked(event) {
        view.setBackgroundColor(event.data);
    }

    // übergibt event an view um header zu erstellen
    function headerButtonClicked(event) {
        view.createNewHeader(event.data);
    }

    //übergibt event an view um text zu erstellen
    function textButtonClicked(event) {
        view.createNewTextfield(event.data);
    }



    //editbar created
    /* function onNavBarCreated(event) {
         var page0btn = document.getElementById("page0"),
             page1btn = document.getElementById("page1"),
             page2btn = document.getElementById("page2");
         page0btn.addEventListener("click", page0btnClicked);
         page1btn.addEventListener("click", page1btnClicked);
         page2btn.addEventListener("click", page2btnClicked);

     }

     function page0btnClicked() {
         view.loadPage(0);
     }

     function page1btnClicked() {
         view.loadPage(1);
     }

     function page2btnClicked() {
         view.loadPage(2);
     }*/

    function onBarCreated(event) {
        var text = event.data[0],
            bar = event.data[1],
            counter = event.data[2],
            down,
            up,
            remove;

        //Variablen füllen
        down = document.getElementById("downbtn" + counter);
        up = document.getElementById("upbtn" + counter);
        remove = document.getElementById("removebtn" + counter);

        // Listener für die einzelnen buttons
        down.addEventListener("click", downButtonClicked);
        up.addEventListener("click", upButtonClicked);
        remove.addEventListener("click", removeButtonClicked);


        //Remove
        function removeButtonClicked() {
            text.remove();
            bar.remove();

        }

        // schiebt ein element nach oben
        function upButtonClicked(event) {
            var actualDiv = event.target.parentElement.parentElement.parentElement,
                prevDiv = actualDiv.previousSibling;
            if (prevDiv != null) {
                document.getElementById("containerDiv").insertBefore(actualDiv, prevDiv);
            }
        }

        // schiebt ein element nach unten
        function downButtonClicked(event) {
            var actualDiv = event.target.parentElement.parentElement.parentElement,
                nextDiv = actualDiv.nextSibling;
            if (nextDiv != null) {
                document.getElementById("containerDiv").insertBefore(nextDiv, actualDiv);
            }
        }
    }


    function init() {
        view = HPGModel.HPGView;
        headerButton = document.getElementById("headerbtn");
        textButton = document.getElementById("textbtn");
        exportButton = document.getElementById("export");
        emailButton = document.getElementById("addmail");
        colorButton = document.getElementById("colorbtn")
            //pageButton = document.getElementById("pagebtn");
        headerButton.addEventListener("click", headerButtonClicked);
        textButton.addEventListener("click", textButtonClicked);
        view.addEventListener("barCreated", onBarCreated);
        // view.addEventListener("navBarCreated", onNavBarCreated);
        exportButton.addEventListener("click", onExportClicked);
        emailButton.addEventListener("click", onMailClicked);
        colorButton.addEventListener("click", onColorButtonClicked);
        // pageButton.addEventListener("click", onPageClicked);
        view.startScripts();

    }


    that.init = init;
    return that;


}());