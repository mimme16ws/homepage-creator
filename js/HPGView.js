HPGModel.HPGView = (function () {
    /* globals HPGModel, EventPublisher*/
    /* env Browser*/

    "use strict";

    var that = new EventPublisher(),
        counter = 0;



    function startScripts() {

        /*
         * User:Adrian B
         * retrieved from http://stackoverflow.com/questions/8100770/auto-scaling-inputtype-text-to-width-of-value
         * Die Textbox des Inputs vom Header-Elemnt wird dynamisch vergrößert    
         */
        // loadPageZero();

        $("body").on("input", "#header", function (event) {
            var target = $(event.currentTarget),
                padding = 10, //Works as a minimum width
                valWidth = target.textWidth() + padding + "px";

            target.css("width", valWidth);
        });

    }

    function setBackgroundColor() {
        var color = document.getElementById("colorPick").value,

            divi = document.getElementById("preview");
        console.log(color);
        divi.style.backgroundColor = color;
    }

    /*function loadPageZero() {
        $(document).ready(function () {
            $(".container").load("page0.html");
        });
    }*/

    /* function createLayout() {
         
         var h = document.querySelector("#navThree-template"),
             t = h.innerHTML,
             x = document.querySelector("#editbar-template"),
             y = x.innerHTML,
             bar = Node.fromString(y.trim()),
             field = Node.fromString(t.trim()),
             removebtn,
             upbtn,
             downbtn,
             newDiv,
             data = [field, bar, counter];

         //Klassen für textfield & textfeld erstellung
         newDiv = document.createElement("div");
         newDiv.setAttribute("id", "Element" + counter);
         field.setAttribute("id", "Textfeld" + counter);

         $(field).children("#editor").addClass("edit" + counter);
         $(field).children(".btn-toolbar").attr("data-target", ".edit" + counter);
         $(field).children(".btn-toolbar").addClass("toolbar" + counter);
         bar.setAttribute("id", "editbar" + counter);

         //bar erstellung
         document.getElementById("preview").appendChild(newDiv);
         document.getElementById("Element" + counter).appendChild(field);
         document.getElementById("Element" + counter).appendChild(bar);

         //Variablen setzen
         removebtn = document.getElementById("remove");
         upbtn = document.getElementById("arrow_Up");
         downbtn = document.getElementById("arrow_Down");

         //namensgebung
         removebtn.setAttribute("id", "removebtn" + counter);
         upbtn.setAttribute("id", "upbtn" + counter);
         downbtn.setAttribute("id", "downbtn" + counter);

         //editor wird gestartet funktioniert nur beim ersten
         $(".edit" + counter).wysiwyg({
             toolbarSelector: ".toolbar" + counter
         });


         //notifier
         that.notifyAll("barCreated", data);
         that.notifyAll("navBarCreated", field);
         counter++;





         //set Linkleiste

              

         //$(document).ready(function () {
           //  $(".container").load("page" + pageCounter + ".html");
         //});
         
     }*/

    /* function loadPage(page) {
         $(document).ready(function () {
             $(".container").load("page" + page + ".html");
         });
         //createLayout();
     }*/

    /*  function openMail() {

          var target = $(event.currentTarget);
          console.log(target[0]);
          var mailto_link = target[0].getAttribute("value");


          window = window.open(mailto_link, 'emailWindow');
          if (window && window.open && !window.closed) {
              window.close();
          }
      }*/

    function createNewMail() {
        var input,
            inputValue,
            newDiv,
            containerDiv,
            email,
            mailto_link,
            a,
            //emailButton,
            removebtn,
            upbtn,
            downbtn,
            x = document.querySelector("#editbar-template"),
            y = x.innerHTML,
            bar = Node.fromString(y.trim()),
            data;

        // Emailadressenerstellung
        input = document.getElementById("emailaddress");
        inputValue = input.value;


        a = document.createElement("a");
        a.href = "mailto:" + inputValue;
        a.innerHTML = "Kontakt";
        a.setAttribute("style", "margin: 0 auto; display:block; text-align: center");
        //Emailbuttonerstellung
        /* emailButton = document.createElement("button");
         emailButton.setAttribute("class", "center-block glyphicon-envelope btn btn-default ");
         emailButton.setAttribute("id", "emailButton" + counter);
         emailButton.innerHTML = " Kontakt";*/

        //Emaillinkerstellung
        /*  email = inputValue;
          mailto_link = 'mailto:' + email;

          emailButton.setAttribute("value", mailto_link);
          emailButton.addEventListener("click", openMail);*/



        //Div erstellung
        newDiv = document.createElement("div");
        containerDiv = document.createElement("div");
        newDiv.setAttribute("id", "Element" + counter);
        containerDiv.setAttribute("id", "containerDiv" + counter);
        containerDiv.classList.add("containerDiv");
        a.setAttribute("id", "emailButton" + counter);

        //editorleiste
        $(a).children("#editor").addClass("edit" + counter);
        $(a).children(".btn-toolbar").attr("data-target", ".edit" + counter);
        $(a).children(".btn-toolbar").addClass("toolbar" + counter);
        bar.setAttribute("id", "editbar" + counter);

        //bar erstellung
        document.getElementById("containerDiv").appendChild(newDiv);
        document.getElementById("Element" + counter).appendChild(containerDiv);
        document.getElementById("containerDiv" + counter).appendChild(a);
        document.getElementById("Element" + counter).appendChild(bar);

        //Variablen setzen
        removebtn = document.getElementById("remove");
        upbtn = document.getElementById("arrow_Up");
        downbtn = document.getElementById("arrow_Down");

        //namensgebung
        removebtn.setAttribute("id", "removebtn" + counter);
        upbtn.setAttribute("id", "upbtn" + counter);
        downbtn.setAttribute("id", "downbtn" + counter);

        data = [a, bar, counter];

        that.notifyAll("barCreated", data);
        counter++;
    }

    // erstellt bei click ein neues textelement
    function createNewTextfield() {

        //Variablen
        var h = document.querySelector("#texteditor-template"),
            t = h.innerHTML,
            x = document.querySelector("#editbar-template"),
            y = x.innerHTML,
            bar = Node.fromString(y.trim()),
            field = Node.fromString(t.trim()),
            removebtn,
            upbtn,
            downbtn,
            newDiv,
            data = [field, bar, counter];

        //Klassen für textfield & textfeld erstellung
        newDiv = document.createElement("div");
        newDiv.setAttribute("id", "Element" + counter);
        field.setAttribute("id", "Textfeld" + counter);

        $(field).children("#editor").addClass("edit" + counter);
        $(field).children(".btn-toolbar").attr("data-target", ".edit" + counter);
        $(field).children(".btn-toolbar").addClass("toolbar" + counter);
        bar.setAttribute("id", "editbar" + counter);

        //bar erstellung
        document.getElementById("containerDiv").appendChild(newDiv);
        document.getElementById("Element" + counter).appendChild(field);
        document.getElementById("Element" + counter).appendChild(bar);

        //Variablen setzen
        removebtn = document.getElementById("remove");
        upbtn = document.getElementById("arrow_Up");
        downbtn = document.getElementById("arrow_Down");

        //namensgebung
        removebtn.setAttribute("id", "removebtn" + counter);
        upbtn.setAttribute("id", "upbtn" + counter);
        downbtn.setAttribute("id", "downbtn" + counter);

        //editor wird gestartet funktioniert nur beim ersten
        $(".edit" + counter).wysiwyg({
            toolbarSelector: ".toolbar" + counter
        });


        //notifier
        that.notifyAll("barCreated", data);
        counter++;
    }

    // erstellt bei click eine neue überschrift
    function createNewHeader() {

        //Variablen
        var h = document.querySelector("#header-editable"),
            t = h.innerHTML,
            x = document.querySelector("#editbar-template"),
            y = x.innerHTML,
            bar = Node.fromString(y.trim()),
            field = Node.fromString(t.trim()),
            removebtn,
            upbtn,
            downbtn,
            newDiv,
            id = "header" + counter,
            data = [field, bar, counter];

        //headererstellung
        newDiv = document.createElement("div");
        newDiv.setAttribute("id", "Element" + counter);
        field.setAttribute("id", id);
        bar.setAttribute("id", "editbar" + counter);
        bar.setAttribute("belongsTo", field.getAttribute("id"));

        //bar erstellung und notifier
        document.getElementById("containerDiv").appendChild(newDiv);
        document.getElementById("Element" + counter).appendChild(field);
        document.getElementById("Element" + counter).appendChild(bar);

        //Variablen setzen
        removebtn = document.getElementById("remove");
        upbtn = document.getElementById("arrow_Up");
        downbtn = document.getElementById("arrow_Down");

        //namensgebung
        removebtn.setAttribute("id", "removebtn" + counter);
        upbtn.setAttribute("id", "upbtn" + counter);
        downbtn.setAttribute("id", "downbtn" + counter);

        //notifier
        that.notifyAll("barCreated", data);
        counter++;
    }

    that.startScripts = startScripts;
    that.createNewTextfield = createNewTextfield;
    that.createNewHeader = createNewHeader;
    that.createNewMail = createNewMail;
    that.setBackgroundColor = setBackgroundColor;
    // that.createLayout = createLayout;
    // that.loadPage = loadPage;
    return that;


})();